;;; ============================================================================
;;; Initialize ELPA (Emacs Lisp Package Archive)
;;; ============================================================================
(when (>= emacs-major-version 24)
  (require 'package)
  (package-initialize)
  (add-to-list 'package-archives '("melpa" . "http://melpa.milkbox.net/packages/") t))

;;; ============================================================================
;;; General stuff
;;; ============================================================================
;; Display clock in modeline
(display-time)
;; Display current column in modeline
(column-number-mode)
;; Don't blink the cursor
(blink-cursor-mode 0)
;; Disable toolbar & scrollbars (for GUI emacs)
(tool-bar-mode -1)
(scroll-bar-mode -1)
;; Disable visual or graphic error bell
(setq ring-bell-function 'ignore)
;; Path to OS executables
(setq exec-path (append exec-path '("/usr/local/bin/")))
(setq exec-path (append exec-path '("/usr/local/texlive/2014/bin/x86_64-darwin/")))
;; Don't use messages that you don't read
(setq initial-scratch-message "")
(setq inhibit-startup-message t)
;; Load Solarized color theme
(load-theme 'solarized-light t)
;; Use auto indent mode
(electric-indent-mode t)
;; Enable HELM
;;(helm-mode t)

;;; ============================================================================
;;; General stuff
;;; ============================================================================
(setq org-list-allow-alphabetical t)
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-cc" 'org-capture)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cb" 'org-iswitchb)

;;; ============================================================================
;;; Use C-c h to do helm-ls-git, for finding files in a git project
;;; ============================================================================
;;(global-set-key (kbd "C-c h") 'helm-ls-git-ls)
(global-set-key (kbd "s-u") 'revert-buffer)

;;; ============================================================================
;;; Fix some character issues with M-x ansi-term
;;; ============================================================================
(defadvice ansi-term (after advise-ansi-term-coding-system)
  (set-buffer-process-coding-system 'utf-8-unix 'utf-8-unix))
(ad-activate 'ansi-term)
;; Use Emacs terminfo, not system terminfo
(setq system-uses-terminfo nil)

;;; ============================================================================
;;; multiple-cursors setup
;;; ============================================================================
(require 'multiple-cursors)
(global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines) ; C-S-c C-S-c to edit block
(global-set-key (kbd "C->") 'mc/mark-next-like-this) ; C-> for the next word
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this) ; C-< for the previous
(global-set-key (kbd "C-c C->") 'mc/mark-all-like-this) ; C-c C-> for all words

;;; ============================================================================
;;; Prevent emacs from putting backup files everywhere
;;; ============================================================================
(setq
 backup-by-copying t ; don't clobber symlinks
 backup-directory-alist
 '(("." . "~/.saves")) ; don't litter my fs tree
 delete-old-versions t
 kept-new-versions 6
 kept-old-versions 2
 version-control t) ; use versioned backups

;;; ============================================================================
;;; Scroll half window height with C-v / M-v
;;; ============================================================================
(defun window-half-height ()
  (max 1 (/ (1- (window-height (selected-window))) 2)))

(defun scroll-up-half ()
  (interactive)
  (scroll-up (window-half-height)))

(defun scroll-down-half ()         
  (interactive)                    
  (scroll-down (window-half-height)))

(global-set-key (kbd "C-v") 'scroll-up-half)
(global-set-key (kbd "M-v") 'scroll-down-half)

;;; ============================================================================
;;; Unset right option key as meta
;;; ============================================================================
(setq ns-right-alternate-modifier nil)

;;; ============================================================================
;;; Bind C-c C-S to magit status
;;; ============================================================================
(global-set-key (kbd "C-c C-s") 'magit-status)

;;; ============================================================================
;;; Set toggle hiding and showing region with C-S-c c
;;; ============================================================================
(require 'hideshow)
(global-set-key (kbd "C-S-c c") 'hs-toggle-hiding)
;;; Set C-c c as compile
(global-set-key (kbd "C-c c") 'compile)

;;; ============================================================================
;;; Add /usr/texbin to PATH
;;; ============================================================================
(setenv "PATH"
	(concat
	 "/usr/texbin" ":"	 
	 (getenv "PATH")))

;;; ============================================================================
;;; Configure web-mode to start automatically for web files
;;; ============================================================================
(require 'web-mode)
(add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))

;;; ============================================================================
;;; Customize web mode
;;; ============================================================================
(require 'web-mode)
(setq web-mode-markup-indent-offset 2)
(setq web-mode-html-indent-offset 2)
(setq-default indent-tabs-mode nil)

;;; ==========================================================================
;;; Don't prompt so much
;;; ==========================================================================
(fset 'yes-or-no-p 'y-or-n-p)
(setq confirm-nonexistent-file-or-buffer nil)
;; Don't ask to kill a terminal with active process
(setq kill-buffer-query-functions
      (remq 'process-kill-buffer-query-function
	    kill-buffer-query-functions))

;;; ==========================================================================
;;; Use ido-mode
;;; ==========================================================================
(ido-mode t)
(ido-everywhere t)
(setq ido-show-dot-for-dired t)

;;; ==========================================================================
;;; Starting Smex (M-x enhancement, like Ido)
;;; ==========================================================================
(autoload 'smex "smex")
(global-set-key (kbd "M-x") 'smex)
(setq smex-save-file "~/.emacs.d/plugin-data/smex/smex-items")

;;; ==========================================================================
;;; Toggle view hidden files
;;; ==========================================================================
(require 'dired-x)
(setq dired-omit-files "^\\...+$")
(add-hook 'dired-mode-hook (lambda () (dired-omit-mode 1)))
(define-key dired-mode-map (kbd "C-c h") 'dired-omit-mode)

;;; ===========================================================================
;;; Custom variables
;;; ===========================================================================
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(LaTeX-command "latex")
 '(TeX-check-path
   (quote
    ("." "/usr/local/texlive/2014/texmf-var/tex/" "/usr/local/texlive/2014/texmf-dist/tex/" "/usr/local/texlive/2014/texmf-dist/bibtex/bst/")))
 '(custom-safe-themes
   (quote
    ("d677ef584c6dfc0697901a44b885cc18e206f05114c8a3b7fde674fce6180879" default)))
 '(exec-path
   ("/usr/bin" "/bin" "/usr/sbin" "/sbin" "/usr/local/Cellar/emacs/24.4/libexec/emacs/24.4/x86_64-apple-darwin14.0.0" "/usr/local/bin/" "/usr/local/texlive/2014/bin/x86_64-darwin/" "/usr/texbin/"))
 '(ghc-core-program "/usr/local/bin/ghc")
 '(haskell-indentation-cycle-warn t)
 '(haskell-indentation-ifte-offset 4)
 '(haskell-indentation-layout-offset 2)
 '(haskell-indentation-left-offset 2)
 '(haskell-indentation-starter-offset 1)
 '(haskell-indentation-where-post-offset 2)
 '(haskell-indentation-where-pre-offset 2)
 '(haskell-mode-hook (quote (turn-on-haskell-doc turn-on-haskell-indent)))
 '(haskell-process-path-cabal "/usr/local/bin/cabal")
 '(haskell-process-path-ghci "/usr/local/bin/ghci")
 '(haskell-process-suggest-add-package t)
 '(haskell-process-suggest-hoogle-imports t)
 '(latex-run-command "pdflatex")
 '(magit-use-overlays nil)
 '(pdf-latex-command "pdflatex")
 '(send-mail-function (quote smtpmail-send-it))
 '(smtpmail-smtp-server "smtp.gmail.com")
 '(smtpmail-smtp-service 587)
 '(vhdl-basic-offset 4)
 '(vhdl-upper-case-keywords t)
 '(vhdl-upper-case-types t))

;;; ============================================================================
;;; Custom faces
;;; ============================================================================
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 99 :width normal :foundry "unknown" :family "DejaVu Sans Mono")))))

