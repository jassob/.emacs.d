;ELC   
;;; Compiled
;;; in Emacs version 24.5.1
;;; with all optimizations.

;;; This file uses dynamic docstrings, first added in Emacs 19.29.

;;; This file does not contain utf-8 non-ASCII characters,
;;; and so can be loaded in Emacs versions earlier than 23.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(byte-code "\300\301!\210\300\302!\210\303\304\305\306\307\310\311\312&\210\313\314\315\316\317DD\320#\210\313\321\315\316\322DD\323#\207" [require json web custom-declare-group el-pocket nil "Pocket" :prefix "el-pocket-" :group external custom-declare-variable el-pocket-oauth-request-url funcall function #[0 "\300\207" ["https://getpocket.com/v3/oauth/request"] 1 "https://getpocket.com/v3/oauth/request\n\n(fn)"] "URL to use for OAuth request." el-pocket-oauth-authorize-url #[0 "\300\207" ["https://getpocket.com/v3/oauth/authorize"] 1 "https://getpocket.com/v3/oauth/authorize\n\n(fn)"] "URL to use for OAuth authorization."] 8)
#@25 Holds the request token
(defvar el-pocket-request-token nil (#$ . 1046))
#@32 Holds the current access token
(defvar el-pocket-access-token-and-username nil (#$ . 1125))
(byte-code "\300\301\302\303\304DD\305#\210\300\306\302\303\307DD\310#\207" [custom-declare-variable el-pocket-consumer-key funcall function #[0 "\300\207" ["30410-da1b34ce81aec5843a2214f4"] 1 "30410-da1b34ce81aec5843a2214f4\n\n(fn)"] "API consumer key" el-pocket-auth-file #[0 "\300\301!\207" [expand-file-name "~/.el-pocket-auth.json"] 2 "\n\n(fn)"] "JSON file to store the authorization."] 5)
(defalias 'el-pocket-load-auth #[0 "\302!\205	 \303!\211\207" [el-pocket-auth-file el-pocket-access-token-and-username file-readable-p json-read-file] 2 "\n\n(fn)"])
(defalias 'el-pocket-save-auth #[0 "\302!\203\n \303!\210\304\305	!\306#\207" [el-pocket-auth-file el-pocket-access-token-and-username file-exists-p delete-file append-to-file json-encode-alist nil] 4 "\n\n(fn)"])
(defalias 'el-pocket-clear-auth #[0 "\302\211\211\207" [el-pocket-request-token el-pocket-access-token-and-username nil] 3 "\n\n(fn)"])
(defalias 'el-pocket-authorize #[0 "\204 \302 \210\203 \303\207	\203 \304 \207\305 \207" [el-pocket-access-token-and-username el-pocket-request-token el-pocket-load-auth t el-pocket-get-access-token el-pocket-get-request-token] 1 "\n\n(fn)" nil])
#@90 After authorizing, el-pocket-authorize again to call this and get an access-token.

(fn)
(defalias 'el-pocket-get-access-token #[0 "\303\304\305\"\303\304\305\"\306\307#\210\306\310	#\210\306\311\312#\210\306\313\314#\210\306\315\316#\210\317\320\321\n\322\323&\266\324\325!\210\326\327!\207" [el-pocket-consumer-key el-pocket-request-token el-pocket-oauth-authorize-url make-hash-table :test equal puthash consumer_key code Host "getpocket.com" Content-type "application/x-www-form-urlencoded; charset=UTF-8" X-Accept "application/json" web-http-post #[771 "\301!\302\303\"\210\304 \207" [el-pocket-access-token-and-username json-read-from-string message "data received is: %s" el-pocket-save-auth] 6 "\n\n(fn CON HEADER DATA)"] :url :data :extra-headers sleep-for 1 display-message-or-buffer "access a-gotten!"] 10 (#$ . 2395)])
#@74 Request a request token, then direct the user to authorization URL

(fn)
(defalias 'el-pocket-get-request-token #[0 "\302C\303\304\305\"\303\304\305\"\306\307#\210\306\310\311#\210\306\312\313#\210\306\314\315#\210\306\316\317#\210\320\321\322\323\324\325!\326\"\327\330%\331	\332\333&\210\334\335!\210\336\337\242\340Q!\207" [el-pocket-consumer-key el-pocket-oauth-request-url nil make-hash-table :test equal puthash consumer_key redirect_uri "http://www.google.com" Host "getpocket.com" Content-type "application/x-www-form-urlencoded; charset=UTF-8" X-Accept "application/json" web-http-post make-byte-code 771 "\302\303\304!\"A\211\300\305	P\240\210\306\300\242!\207" vconcat vector [el-pocket-request-token assoc code json-read-from-string "https://getpocket.com/auth/authorize?request_token=" kill-new] 7 "\n\n(fn CON HEADER DATA)" :url :data :extra-headers sleep-for 1 display-message-or-buffer "authorize el-pocket at " " (copied to clipboard)\n"] 11 (#$ . 3247)])
#@30 Do we have access yet?

(fn)
(defalias 'el-pocket-access-granted-p #[0 "\205 \301\207" [el-pocket-access-token-and-username t] 1 (#$ . 4242)])
(defalias 'el-pocket-access-not-granted #[0 "\300\301!\207" [display-message-or-buffer "Do an M-x el-pocket-authorize to get access to pocket."] 2 "\n\n(fn)"])
#@37 Gets things from your pocket.

(fn)
(defalias 'el-pocket-get #[0 "\302 \203H \303\304\305\"\303\304\305\"\306\307#\210\306\310\311\310	\"A#\210\306\312\313#\210\306\314\315#\210\306\316\317#\210\306\320\321#\210\306\322\323#\210\324\325\326\327\330\331&\207\332 \207" [el-pocket-consumer-key el-pocket-access-token-and-username el-pocket-access-granted-p make-hash-table :test equal puthash consumer_key access_token assoc count "5" detailType "simple" Host "getpocket.com" Content-type "application/x-www-form-urlencoded; charset=UTF-8" X-Accept "application/json" web-http-post #[771 "\300\301\"\210\302!\207" [message "data received is: %s" json-read-from-string] 6 "\n\n(fn CON HEADER DATA)"] :url "https://getpocket.com/v3/get" :data :extra-headers el-pocket-access-not-granted] 10 (#$ . 4553)])
#@49 Add URL-TO-ADD to your pocket.

(fn URL-TO-ADD)
(defalias 'el-pocket-add #[257 "\302 \203B \303\304\305\"\303\304\305\"\306\307#\210\306\310\311\310	\"A#\210\306\312#\210\306\313\314#\210\306\315\316#\210\306\317\320#\210\321\322\323\324\325\326&\207\327 \207" [el-pocket-consumer-key el-pocket-access-token-and-username el-pocket-access-granted-p make-hash-table :test equal puthash consumer_key access_token assoc url Host "getpocket.com" Content-type "application/x-www-form-urlencoded; charset=UTF-8" X-Accept "application/json" web-http-post #[771 "\300\301\"\210\302!\207" [message "data received is: %s" json-read-from-string] 6 "\n\n(fn CON HEADER DATA)"] :url "https://getpocket.com/v3/add" :data :extra-headers el-pocket-access-not-granted] 11 (#$ . 5375) (byte-code "\300\301!C\207" [read-string "el-pocket url: "] 2)])
(provide 'el-pocket)
